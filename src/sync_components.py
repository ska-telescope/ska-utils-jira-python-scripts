# from asyncio.log import logger
from dev_logging import logger
from pprint import pformat as pf
from pprint import pprint
from ska_cicd_services_api import jira
import asyncio
import yaml
import csv
import os

async def main():

    jira_obj = jira.SKAJira()

    await jira_obj.authenticate()

    # ####################################
    # # SYNC PROJECT COMPONENTS STARTS HERE
    # example of env variable: SYNC_PROJECTS=CHR RSS
    list_of_projects = os.getenv("SYNC_PROJECTS").split()

    logger.debug(f"Environment sets list of projects as: {list_of_projects}")

    projects_list = await jira_obj.get_list_of_projects(projects_list=list_of_projects)
    log=f"PROJECTS:\n{projects_list}\n"
    logger.warning(pf(log))

    components_all_projects_jira = await jira_obj.get_components_for_multiple_projects(
        projects_list=list_of_projects
    )

    for project in components_all_projects_jira:
        for component in project:
            logger.info(f"SINGLE COMPONENT: {component.name}: {component.id} ({component.project})")
            
    # Import all data from single data source
    filename = "data/ALIMExportedPBS.yaml"
    all_components_main_list = []
    with open(filename, "r") as data:
        for line in yaml.safe_load(data):
            all_components_main_list.append(line)
    logger.debug(f"There are {len(all_components_main_list)} components registered")


    # ####################################
    # Main sync function
    await jira_obj.create_or_update_all_components_for_multiple_projects(
        project_list=list_of_projects, component_info_list=all_components_main_list#, update_existing=False
    )
    # ####################################


if __name__ == "__main__":

    import sys
    print(sys.executable)
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    ### LIVE GRENADES
    # fn = "data/lots_of_issues.csv"
    # jira_obj.bulk_create_issues_from_csv(fn)

