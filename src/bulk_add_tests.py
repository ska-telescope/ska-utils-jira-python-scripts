# from asyncio.log import logger
from dev_logging import logger
from pprint import pformat as pf
from pprint import pprint
from jira import JIRA
from atlassian import Xray
import os
import csv

def field_id(jira):
    # Fetch all fields
    allfields = jira.fields()
    # Make a map from field name -> field id
    nameMap = {field['name']:field['id'] for field in allfields}

def field_id_for_issue(jira,custom_name='Test Type', issue_key):
    # Fetch an issue
    issue = jira.issue(issue_key)
    # You can now look up custom fields by name using the map
    getattr(issue.fields, nameMap[custom_name])


def main():
    jira_url = os.environ["JIRA_URL"]
    user = os.environ["JIRA_USERNAME"]
    password = os.environ["JIRA_PASSWORD"]

    jira = JIRA(server=jira_url, basic_auth=(user,password))
    myself = jira.myself()
    logger.info(f">>> {myself['name']} is authenticted")

    xr = Xray(
        url=jira_url,
        username=user,
        password=password)

    features = jira.search_issues(jql_str="project='SAFe Program' AND fixVersion = PI15 and status IN ('Program Backlog', Releasing, DONE, Implementing) AND 'Shared Teams' >0")

    # #################
    # # ADD TESTS
    # #################
    ### LIVE GRENADES

    all_new_test_keys = []
    for feature in features:
        logger.info(f"{feature.key} enriched:")

        # create Test
        new_test = jira.create_issue(test_issue())
        logger.info(f"New Test created: {jira_url}/browse/{new_test.key}")
        new_test.update(fields={field_id():"Generic"})
        all_new_test_keys.append(new_test.key)

        # create Test Set and add new_test
        new_test_set = jira.create_issue(test_set_issue())
        logger.info(f"New Test Set created: {new_test_set.key}")
        xr.update_test_set(test_set_key=new_test_set.key,add=[new_test.key])
        logger.info(f"Test {new_test} added to Test Set {new_test_set.key}")
        
        # link Test Set to Feature
        jira.create_issue_link(type="Tests",inwardIssue=new_test_set.key, outwardIssue=feature.key)
        logger.info(f"{new_test_set.key} now tests {feature.key}")
    xr.update_test_plan(test_plan_key="RSS-708",add=all_new_test_keys)

def test_issue():
    """Test Issue contents

    Returns:
        dict: Dictionary with template data for Compliance Test Issue
    """
    return {
            "summary": "Requirement Coverage Complete",
            "project": {
                "key": "XTP"
            },
            "description": "h2. Generic Test Verifying Test Coverage of Feature\nThis is a generic test that only involves inspection of all the Tests associated with the linked Requirement, Enabler or Feature.\nIf the Test Engineer/Feature Owner inspecting the tests that verify the Requirement is satisfied that these Tests cover the Requirement in full, then they may manually execute the test and record it as passing.\nh2. NOTES\n* The Test Execution *must* have the Fix Version set to the current PI.\n* The Test Definition needs to be done by the Feature Owner and may contain any details such as what areas should be covered specifically, if necessary. This field is not mandatory and its use is for the discretion of the Feature Owner/Test Engineer",
            "issuetype": {
                "name": "Test"
            },
        }

def test_set_issue():
    """Test Set contents

    Args:
        test_issue (_type_): _description_

    Returns:
        _type_: _description_
    """
    return {
            "summary": "Compliance Checklist",
            "project": {
                "key": "XTP"
            },
            "description": "h2. Feature Compliance Checklist\nThis is a Test Set that can be adjusted by adding or removing tests to the Test Set. It is associated with one particular Feature and can be used to group any tests that verify the completeness of the Feature.\nThis Test Set can be used as an alternative method for checking completeness of a Feature against the Definition of Done. As a starting point, the Test Set contains one test that is used to verify the Coverage of the Feature.\n\nDetails on how to execute these manual tests are available [here|https://confluence.skatelescope.org/x/Fy1wCg].",
            "issuetype": {
                "name": "Test Set"
            },
        }
    
if __name__ == "__main__":

    main()
